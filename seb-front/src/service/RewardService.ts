import Service from './Service';

const baseUrl = 'http://localhost:3000/reward';

const RewardService = new Service(baseUrl);

export default RewardService;