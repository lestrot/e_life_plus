import { INormalizedData } from '../../../seb-model/src/interface/INormalizedData';
import { IRole } from '../../../seb-model/src/interface/IRole';
import { IUser } from '../../../seb-model/src/interface/IUser';
import { Identified } from '../../../seb-model/src/type/Identified';
import { filter } from '../utils';

interface IStoreState {
    users : INormalizedData<IUser>;
    roles : INormalizedData<IRole>;
}

export const getPartners = (state : IStoreState) : Identified<IUser>[] => {
    const partnerRole = filter(state.roles)(role => role.name === 'ROLE_PARTNER');
    return filter(state.users)(user => user.idRole === partnerRole[0].id);
};