import { IMold } from '../../../seb-model/src/interface/IMold';
import { INormalizedData } from '../../../seb-model/src/interface/INormalizedData';
import { IRecovery } from '../../../seb-model/src/interface/IRecovery';
import { IUser } from '../../../seb-model/src/interface/IUser';
import { Identifier } from '../../../seb-model/src/type/Identifier';
import { listUserDeliveredMolds } from './listUserDeliveredMolds';

interface IStoreState {
    users : INormalizedData<IUser>;
    molds : INormalizedData<IMold>;
    recoveries : INormalizedData<IRecovery>;
}

export const getUserTotalDeliveredMold = (state : IStoreState) => (idUser : Identifier) : number => {
    return listUserDeliveredMolds(state)(idUser).length;
};