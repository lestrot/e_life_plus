import { IAddress } from '../../../seb-model/src/interface/IAddress';
import { INormalizedData } from '../../../seb-model/src/interface/INormalizedData';

export const SetAddresses = 'address/set-addresses';

export const setAddresses = (addresses : INormalizedData<IAddress>) => {
    return {
        type : SetAddresses,
        payload : { addresses }
    };
};