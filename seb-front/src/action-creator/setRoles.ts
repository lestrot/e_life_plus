import { INormalizedData } from '../../../seb-model/src/interface/INormalizedData';
import { IRole } from '../../../seb-model/src/interface/IRole';

export const SetRoles = 'roles/set-roles';

export const setRoles = (roles : INormalizedData<IRole>) => {
    return {
        type : SetRoles,
        payload : { roles }
    };
};