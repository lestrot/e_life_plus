export const postData = (body) => {
    return {
        method : 'POST',
        headers : {
            'Content-Type' : 'application/json',
            'Authorization' : `Bearer ${ localStorage.getItem('token') }`
        },
        body : JSON.stringify(body)
    };
};