import { expect, should, use } from 'chai';
import chaiAsPromised from 'chai-as-promised';
import httpStatus from 'http-status';
import request from 'supertest-as-promised';
import app from '../../index';
import { getPartnerPartnerRecoveries } from '../../src/helper/partnerRecoveryHelpers';
import { getAllRecoveries } from '../../src/helper/recoveryHelpers';
import { getPartner } from '../../src/helper/userHelpers';
import { IPartnerRecovery } from '../../src/interface/IPartnerRecovery';
import { IRecovery } from '../../src/interface/IRecovery';
import { IUser } from '../../src/interface/IUser';
import { Identified } from '../../src/type/Identified';

should();
use(chaiAsPromised);
describe('Recoveries test Suite', () => {
    describe('/POST recoveries', () => {
        it('should add recovery to Partner recoveries list', async () => {
            const recoveries : Identified<IRecovery>[] = await getAllRecoveries();
            const partner : Identified<IUser> = await getPartner();
            const initial : Identified<IPartnerRecovery>[] = await getPartnerPartnerRecoveries(partner.id);
            request(app)
                .post('/recovery/pick')
                .send({
                    idRecovery : recoveries[0].id,
                    idUser : partner.id
                })
                .expect(httpStatus.OK)
                .then(async () => {
                    const final : Identified<IPartnerRecovery>[] = await getPartnerPartnerRecoveries(partner.id);
                    expect(final.length).to.equal(initial.length + 1);
                });
        });
        it('should remove recovery Partner recoveries list', async () => {
            const partner : Identified<IUser> = await getPartner();
            const initial : Identified<IPartnerRecovery>[] = await getPartnerPartnerRecoveries(partner.id);
            request(app)
                .post('/recovery/leave')
                .send({
                    idPartnerRecovery : initial[0].id,
                })
                .expect(httpStatus.OK)
                .then(async () => {
                    const final : Identified<IPartnerRecovery>[] = await getPartnerPartnerRecoveries(partner.id);
                    expect(final.length).to.equal(initial.length - 1);
                });
        });
    });

});
