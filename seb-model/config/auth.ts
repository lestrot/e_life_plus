import jsonWebToken from 'jsonwebtoken';
import { getUserById, getUserRole } from '../src/helper/userHelpers';
import { IRole } from '../src/interface/IRole';

export default class Auth {
    static auth(roles) {
        return async (req, res, next) => {
            try {
                const token = req.headers.authorization.replace(/Bearer /g, '');
                const decryptToken = jsonWebToken.decode(token, 'mysecret94652');
                const user = await getUserById(decryptToken.sub);
                const role : IRole = await getUserRole(user.idRole);
                if (user && roles.includes(role.name)) {
                    next();
                } else {
                    res.status(401).json({ 'message' : 'Unauthorized' });
                }

            } catch (error) {
                res.status(403).json({ 'message' : error });
            }
        };
    }
}